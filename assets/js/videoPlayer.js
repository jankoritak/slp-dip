/**
 * Created by coreey on 23. 11. 2015.
 */
/*
 * TODO LIST:
 * -------------------------------------
 * Input type range for changing volume?
 * Test whether buffering interaction in playOrPause method works properly
 * I don't like clicking repeatedly to change playback rate - maybe dragbar?
 * Dont forget to add touchStart to every click event for mobile devices
 * Initial playback rate value cannot be set in HTML file
 */

var VideoPlayer = function() {
    var self = this;
    this.player = null;
    this.ajax = null;
    this.slidesPlayer = null;

    this.playerInstance = jwplayer("video-player");

    this.setup = function(player, playerWidth) {
        this.player = player;
        this.ajax = player.ajax;
        this.slidesPlayer = player.slidesPlayer;

        //Initialize JW Player instance
        this.playerInstance.setup({
            primary : 'html5',
            file : this.ajax.getVideo(),
            aspectratio : '4:3',
            width: playerWidth,
            controls : false,
        });

        //Initialize whole player
        this.playerInstance.setVolume(50);

        //Events
        $('#play, #pause, #replay ').on('click', this.playback);
        $('#playback-rate').on('click', this.updatePlaybackRate);
        $('#mute, #unmute').on('click', this. volume);
        $('#volume-bar-container').on('click', this.updateVolume);
        $('#progress-bar-panel').on('click', this.rewind);

        //Intervals
        setInterval(this.updatePlayer, 1000);
    }

    /*
     * Updates everything that changes periodically (in interval)
     * Updates: Time, Progressbar, Complete
     */
    this.updatePlayer = function() {
        /* Time */
        var seconds = Math.round(self.playerInstance.getPosition());
        var minutes = Math.floor(seconds/60);

        if(seconds > 0)
            seconds -= minutes*60;

        if(seconds.toString().length == 1)
            seconds = '0' + seconds;

        var totalSeconds = Math.round(self.playerInstance.getDuration());
        var totalMinutes = Math.floor(totalSeconds/60);

        if(totalSeconds > 0)
            totalSeconds -= totalMinutes*60;

        if(totalSeconds.toString().length == 1)
            totalSeconds = '0' + totalSeconds;

        $('#time').text(minutes + ':' + seconds + ' / ' + totalMinutes + ':' + totalSeconds);

        /* Progress bar - video played*/
        var progressBarPlayer = (self.playerInstance.getPosition()/self.playerInstance.getDuration())*100;
        $('#progress-bar-played').css('width', progressBarPlayer + '%');

        /* Progress bar - video buffered */
        $('#progress-bar-buffered').css('width', self.playerInstance.getBuffer() + '%');

        /* Replay button */
        if(self.playerInstance.getState() == 'complete') {
            self.player.updateButton(self.player.buttons['replay']);
        }

        /* Log event */
        self.player.logEvent(self.playerInstance.getState());
    }

    /*
     * Play/Pause/Replay click event handler
     */
    this.playback = function() {
        var state = self.playerInstance.getState();

        /*
         * Idle - before playing
         * Complete - after finishing playing, used for replay
         */
        if(state == 'idle' || state == 'paused' || state == 'complete') {
            self.playerInstance.play();
            self.player.updateButton(self.player.buttons['pause']);
        }
        else if(state == 'playing' || state == 'buffering'){
            self.playerInstance.pause();
            self.player.updateButton(self.player.buttons['play']);
        }

        /* Log event */
        self.player.logEvent(this);
    }

    this.updatePlaybackRate = function() {
        var video = document.querySelector('video');
        var playbackRateObject = $('#playback-rate');
        var rate = video.playbackRate;

        (rate < 1.5) ? rate += 0.1 : rate = 1;

        rate = (Math.floor(rate * 100) / 100).toFixed(1);

        video.playbackRate = rate;
        playbackRateObject.text(rate + ' x');

        /* Log event */
        self.player.logEvent(this);
    }

    /*
     * Mute/unmute toggle function
     * Effect for clarity - shrink volumeBar to 0 on mute
     */
    this.volume = function() {
        var volumeBar = $('#volume-bar');

        if(self.playerInstance.getMute()) {
            self.playerInstance.setMute(false);
            self.player.updateButton(self.player.buttons['mute']);

            volumeBar.css('width', self.playerInstance.getVolume() + '%');
        }
        else {
            self.playerInstance.setMute(true);
            self.player.updateButton(self.player.buttons['unmute']);

            volumeBar.css('width', 0);
        }

        /* Log event */
        self.player.logEvent(this);
    }

    /*
     * Counts nad updates volume as a reaction to volume bar click
     * @param e - click event object
     */
    this.updateVolume = function(e) {
        /* Unmute if muted */
        if(self.playerInstance.getMute()) {
            self.playerInstance.setMute(false);
            self.player.updateButton(self.player.buttons['mute']);
        }

        /* Count volume */
        var volumeBarContainer = $('#volume-bar-container');
        var volumeBar = $('#volume-bar');

        var mouseX = e.pageX - volumeBarContainer.offset().left;
        var width = volumeBarContainer.css('width');
        width = parseFloat(width.substr(0, width.length - 2));

        var volume = Math.round((mouseX/width)*100);

        if(volume < 0) volume = 0;
        else if (volume > 100) volume = 100;

        /* Set volume */
        self.playerInstance.setVolume(volume);
        volumeBar.css('width', volume + '%');

        /* Log event */
        self.player.logEvent(this);
    }

    this.rewind = function(e) {
        var progressBarContainer = $('#progress-bar-panel');
        var progressBar = $('#progress-bar-played');

        /* Count seek */
        var mouseX = e.pageX - progressBarContainer.offset().left;
        var width = progressBarContainer.css('width');
        width = parseFloat(width.substr(0, width.length - 2));

        var percentage = mouseX/width;

        var time = Math.round((self.playerInstance.getDuration()*percentage));

        /* Seek */
        self.playerInstance.seek(time);

        /* If paused before seek, now play */
        if(self.playerInstance.getState() == 'paused')
            $('#play').trigger('click');

        progressBar.css('width', percentage*100 + '%');

        /* Change slide */
        if(self.slidesPlayer != null)
            self.slidesPlayer.syncSlidesToRewind(time);

        /* Log event */
        self.player.logEvent(this);
    }

    this.syncVideoToRewind = function(time) {
        this.playerInstance.seek(time);
    }

    this.getPlayerInstance = function() {
        return this.playerInstance;
    }
}