/**
 * Created by coreey on 7. 12. 2015.
 */

var Player = function(config) {
    var self = this;

    this.config = config;

    this.ajax = null;
    this.slidesPlayer = null;
    this.videoPlayer = null;

    /* Resize attributes */
    this.isFullscreen = false;

    //Buttons
    this.buttons = {
        play : {
            id : 'play',
            group : 'playback',
            caption : 'Play',
            icon : 'glyphicon glyphicon-play'
        },
        pause : {
            id : 'pause',
            group : 'playback',
            caption : 'Pause',
            icon : 'glyphicon glyphicon-pause'
        },
        replay : {
            id : 'replay',
            group : 'playback',
            caption : 'Replay',
            icon : 'glyphicon glyphicon-repeat'
        },
        mute : {
            id : 'mute',
            group : 'volume',
            caption: 'Volume',
            icon : 'glyphicon glyphicon-volume-up'
        },
        unmute : {
            id : 'unmute',
            group : 'volume',
            caption: 'Muted',
            icon : 'glyphicon glyphicon-volume-off'
        },
        camera : {
            id : 'camera',
            group : 'camera',
            caption: 'Camera',
            icon : 'glyphicon glyphicon-facetime-video'
        },
        maximize : {
            id : 'maximize',
            group : 'resize',
            caption: 'Maximize',
            icon : 'glyphicon glyphicon-resize-full'
        },
        minimize : {
            id : 'minimize',
            group : 'resize',
            caption: 'Minimize',
            icon : 'glyphicon glyphicon-resize-small'
        }
    }

    this.setup = function() {
        var videoPanelElement = $('#video-panel');
        var slidesPanelElement = $('#slides-panel');
        var slidesBarElement = $('#slides-bar');

        this.ajax = new Ajax(this.config.lectureID);
        this.ajax.setup(function(hasSlides) {
            self.videoPlayer = new VideoPlayer();

            if(hasSlides) {
                /* Shrink video panel width to half */
                videoPanelElement.removeClass('full');
                videoPanelElement.addClass('half');

                /* Show slides */
                slidesPanelElement.removeClass('hide');

                /* Show slides bar */
                slidesBarElement.removeClass('hide');

                self.slidesPlayer = new SlidesPlayer();
                self.slidesPlayer.setup(self);
            }

            /* Has slides - video fills whole video panel
             * Does not have slides - video fills half of video panel (shrink for preview)
             */
            var videoPlayerWidth = (hasSlides) ? '100%' : '50%';
            self.videoPlayer.setup(self, videoPlayerWidth);
        });

        /* Resize player event */
        $(window).resize(function() {
            if(self.isFullscreen)
                self.updatePlayer();
        });

        $(window).on("orientationchange", function( event ) {
            if(self.isFullscreen)
                self.updatePlayer();
        });

        $('#maximize, #minimize').on('click', this.fullscreen);
    }

    this.fullscreen = function() {
        var playerElement = $('#player');
        var videoPanel = $('#video-panel');
        var slidesPanel = $('#slides-panel');
        var slide = $('#slide');

        if(!self.isFullscreen) {
            self.isFullscreen = true;

            playerElement.addClass('fullscreen');
            self.updateButton(self.buttons['minimize']);

            self.updatePlayer();
        }
        else {
            self.isFullscreen = false;
            playerElement.removeClass('fullscreen');
            self.updateButton(self.buttons['maximize']);

            if(self.slidesPlayer != null) {
                slide.css('height', 'auto');
                slide.css('width', 'auto');

                videoPanel.css('width', '50%');
                videoPanel.css('height', 'auto');

                slidesPanel.css('width', '50%');
                slidesPanel.css('height', 'auto');

                self.videoPlayer.playerInstance.resize('100%', 'auto');

                videoPanel.css('padding-top', 0);
                slidesPanel.css('padding-top', 0);
            }
            else {
                videoPanel.css('width', '100%');
                videoPanel.css('height', 'auto');

                self.videoPlayer.playerInstance.resize('50%', 'auto')
            }
        }

        /* Log event */
        self.logEvent(this);
    }

    this.updatePlayer = function() {
        var videoPanel = $('#video-panel');
        var slidesPanel = $('#slides-panel');
        var slide = $('#slide');

        var height = this.getUsableHeight();

        var videoPaddingTop = 0;
        var slidePaddingTop = 0;

        if(this.slidesPlayer != null) {
            /*
             * Portratit orientation
             * Video sits on top of slides
             */
            if(!this.getOrientation()) {
                height /= 2;
                /*
                 * JWPlayer in responsive mode requires width to be set, height is ignored
                 * We are using 4:3 ratio, so count the width from height using ratio knowledge
                 * width = height/2/3*4
                 * The ternary operator is used for situation, where we need to keep aspect ratio, but screen is
                 * way too small for that
                 * */
                var width = ($(window).innerWidth() < height/3*4) ? $(window).innerWidth() : height/3*4;

                /* Video */

                videoPanel.css('width', '100%');
                videoPanel.css('height', height);

                this.videoPlayer.playerInstance.resize(width, 'auto');

                /* Slides */

                slidesPanel.css('width', '100%');
                slidesPanel.css('height', height);

                /*
                 * Fit according to width
                 * Needs to be reset to auto when out of portrait/fullscreen
                 */
                slide.css('height', 'auto');
                slide.css('width', width);

                /*
                 * Slides
                 * Note for author: Changed  from height/3*4!
                 */
                if($(window).innerWidth() < width) {
                    videoPaddingTop = (height - this.videoPlayer.playerInstance.getHeight())/2;
                    slidePaddingTop = (height - slide.height())/2;
                }
                else {
                    videoPaddingTop = 0;
                    slidePaddingTop = 0;
                }

                videoPanel.css('padding-top', videoPaddingTop);
                slidesPanel.css('padding-top', slidePaddingTop);
            }
            else {
                /*
                 * Video/slides width
                 * Cover atypic wide screens
                 */
                var width = ($(window).innerWidth()/2 < height/3*4) ? $(window).innerWidth()/2 : height/3*4;

                /* Video */

                videoPanel.css('width', '50%');
                videoPanel.css('height', height);

                this.videoPlayer.playerInstance.resize(width, 'auto');

                /* Slides */

                slidesPanel.css('width', '50%');
                slidesPanel.css('height', height);

                slide.css('height', 'auto');
                slide.css('width', width);

                /* Padding */

                videoPaddingTop = (height - this.videoPlayer.playerInstance.getHeight())/2;
                slidePaddingTop = (height - slide.height())/2;

                videoPanel.css('padding-top', videoPaddingTop);
                slidesPanel.css('padding-top', slidePaddingTop);
            }
        }
        else {
            var width = ($(window).innerWidth() < height/3*4) ? $(window).innerWidth() : height/3*4;

            videoPanel.css('width', '100%');
            videoPanel.css('height', height);

            this.videoPlayer.playerInstance.resize(width, 'auto');

            if($(window).innerWidth() < height/3*4)
                videoPaddingTop = (height - this.videoPlayer.playerInstance.getHeight())/2;
            else
                videoPaddingTop = 0;

            videoPanel.css('padding-top', videoPaddingTop);
        }
    }

    this.getOrientation = function() {
        var windowHeight = $(window).innerHeight();
        var windowWidth = $(window).innerWidth();

        /* Landscape mode == true */
        if(windowWidth > windowHeight) {
            return true;
        }
        /* Porait mode == false */
        else {
            return false;
        }
    }

    /*
     * Sets buttons icon and caption according to params
     * newButton - button object
     */
    this.updateButton = function(newButton) {
        var newButtonObject = $('#' + newButton['id']);

        $.each(this.buttons, function(i, e) {
            if(e['group'] === newButton['group']) {
                $('#' + e['id']).addClass('hide');
            }
        });

        newButtonObject.removeClass('hide');
    }

    this.getUsableHeight = function() {
        var slidesBarHeight = $('#slides-bar').height();
        var progressBarPanelHeight = $('#progress-bar-panel').height();
        var controlPanelHeight = $('#control-panel').height();

        if(this.slidesPlayer != null)
            return $(window).innerHeight() - (slidesBarHeight + progressBarPanelHeight + controlPanelHeight);
        else
            return $(window).innerHeight() - (progressBarPanelHeight + controlPanelHeight);
    }

    this.logEvent = function(event) {
        console.log(event)
    }

    this.getIsFullscreen = function() {
        return this.isFullscreen;
    }
}