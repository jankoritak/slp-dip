/**
 * Created by coreey on 19. 12. 2015.
 */

var Ajax = function(lectureID) {
    var self = this;

    this.lecture = null;

    this.lectureID = lectureID;
    //speechprosody2014
    this.url = "http://www.superlectures.com/barcampbrno2015/api/getLecture?id=";

    this.setup = function(callback) {
        $.ajax({
            type: "GET",
            url: this.url + this.lectureID,
            dataType: "jsonp",
            async : false,
            success: function (data) {
                self.lecture = data.lecture;
                callback(self.hasSlides());
            }
        });
    }

    this.getVideo = function() {
        return this.lecture['video']['video_mp4_hd']['@attributes']['src'];
    }

    this.hasSlides = function() {
        return (this.lecture['slides']['@attributes']['number_of_slides'] == 0) ? false : true;
    }

    this.getSlides = function() {
        return this.lecture['slides']['slide'];
    }
}