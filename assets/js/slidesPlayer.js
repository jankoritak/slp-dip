/**
 * Created by coreey on 19. 12. 2015.
 */

var SlidesPlayer = function() {
    var self = this;
    this.player = null;
    this.ajax = null;
    this.videoPlayer = null;

    this.slides = null;
    this.nextSlide = null;
    this.lastSlide = null;
    this.nextSlideTime = null;

    this.setup = function(player) {
        this.player = player;
        this.ajax = player.ajax;
        this.videoPlayer = player.videoPlayer;

        this.slides = this.ajax.getSlides();

        this.nextSlide = 0;
        this.lastSlide = this.slides.length - 1;
        this.nextSlideTime = this.slides[this.nextSlide]['@attributes']['start'];

        this.genSlidesBar();
        this.syncSlidesToTime();

        $('.slides-bar-slide-container').on('click', function() {
            self.rewind($(this).attr('id'));
        });

        setInterval(function() {
            if(self.videoPlayer.getPlayerInstance().getPosition() >= self.getNextSlideTime()) {
                self.syncSlidesToTime();
            }
        }, 1000);
    }

    this.syncSlidesToTime = function() {
        this.updateSlide();

        if(this.nextSlide != this.lastSlide) {
            this.nextSlide += 1;
            this.nextSlideTime = this.slides[this.nextSlide]['@attributes']['start'];
        }
    }

    this.syncSlidesToRewind = function(time) {
        $.each(this.slides, function(index, slide) {
            var slideTime = slide['@attributes']['start'];

            if(slideTime >= time) {
                self.nextSlide = (index == 0) ? 0 : index-1;
                self.nextSlideTime = slideTime;
                return false;
            }
        });

        this.updateSlide();
    }

    this.updateSlide = function() {
        var slidesPlayerElement = $('#slides-panel');
        var slideHTML = '<img id="slide" src="' + this.slides[this.nextSlide]['@attributes']['src_large'] + '" />';;

        slidesPlayerElement.html(slideHTML);

        if(self.player.getIsFullscreen())
            self.player.updatePlayer();
    }

    this.rewind = function(slideID) {
        var index = parseInt(slideID.substr(6));
        var slide = this.slides[index]['@attributes'];

        self.nextSlide = index;
        self.nextSlideTime = this.slides[index]['@attributes']['start'];

        self.videoPlayer.syncVideoToRewind(slide['start']);
    }

    this.genSlidesBar = function() {
        var slidesBar = $('#slides-bar');

        $.each(this.slides, function(index, slide) {
            var slideContainerSpan = $('<span>', {
                id : 'slide-' + index,
                class : 'slides-bar-slide-container'
            });

            var slideDiv = $('<img>', {
                src : slide['@attributes']['src_large'],
                height : slidesBar.height()
            });

            slidesBar.append(slideContainerSpan.html(slideDiv));
        });
    }

    this.getNextSlideTime = function() {
        return this.nextSlideTime;
    }
}